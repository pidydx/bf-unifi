#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    mkdir -p /var/lib/unifi/data
    mkdir -p /var/lib/unifi/logs
    mkdir -p /var/lib/unifi/db
    cp /etc/unifi/system.properties /var/lib/unifi/data/system.properties
    if [ ! -f /var/lib/unifi/data/system.properties.bk ]; then
        echo "is_default=true" >> /var/lib/unifi/data/system.properties
    else
        echo "is_default=false" >> /var/lib/unifi/data/system.properties
        grep "uuid" /var/lib/unifi/data/system.properties.bk >> /var/lib/unifi/data/system.properties
    fi
    exec echo "UniFi Controller Initialized!"
fi

if [ "$1" = 'unifi' ]; then
    exec /usr/bin/java -Xmx1024M \
    -Djava.awt.headless=true \
    -Dfile.encoding=UTF-8 \
    -Djava.io.tmpdir=/usr/local/UniFi/tmp \
    -Dorg.xerial.snappy.use.systemlib=true \
    --add-opens java.base/java.time=ALL-UNNAMED \
    -jar /usr/local/UniFi/lib/ace.jar start
fi

if [ "$1" = 'mongo' ]; then
    exec mongod -f /etc/mongo/mongod.conf --dbpath /var/lib/mongodb/db
fi

exec "$@"