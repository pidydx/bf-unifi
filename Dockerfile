#########################
# Create base container #
#########################
FROM ubuntu:22.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=unifi
ENV APP_GROUP=unifi

ENV UNIFI_VERSION=8.4.62

ENV BASE_PKGS curl libsnappy-java openjdk-17-jre-headless mongodb-enterprise-server

# Create app user and group
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -m -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /var/lib/unifi -s /usr/sbin/nologin

# Update and install base packages
COPY etc/apt /etc/apt

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget unzip

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src
RUN wget -nv https://dl.ui.com/unifi/${UNIFI_VERSION}/UniFi.unix.zip
RUN unzip -q UniFi.unix.zip -d /usr/local
RUN mkdir -p /usr/local/UniFi/run
RUN mkdir -p /usr/local/UniFi/tmp


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN chown -R ${APP_USER}:${APP_GROUP} /usr/local/UniFi
EXPOSE 3478/udp 6789/tcp 8080/tcp 8443/tcp 8843/tcp 8880/tcp 10001/udp
VOLUME ["/var/lib/unifi", "/etc/unifi", "/usr/local/UniFi/data", "/usr/local/UniFi/logs", "/var/lib/mongodb", "/etc/mongo"]

WORKDIR /usr/local/UniFi
USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["unifi"]
